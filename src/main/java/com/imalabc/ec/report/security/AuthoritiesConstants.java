package com.imalabc.ec.report.security;

public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String LABORATORIO = "ROLE_LABORATORY";

    public static final String ANALYST = "ROLE_ANALYST";

    private AuthoritiesConstants() {
    }

}
