package com.imalabc.ec.report.repository;

import com.imalabc.ec.report.domain.CuerpoCuestionario;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class CuerpoCuestionarioRepository implements PanacheRepository<CuerpoCuestionario> {

    public List<CuerpoCuestionario> findBodyByIdHead(Long identificador){
        List<CuerpoCuestionario> out = list("cuerpoCuestionarioPK.cuestionario", identificador)
                .stream().filter(CuerpoCuestionario::isMarca).collect(Collectors.toList());
        out.sort(new SortCuerpoCuestionarioByName());
        return out;
    }

}

class SortCuerpoCuestionarioByName implements Comparator<CuerpoCuestionario> {

    @Override
    public int compare(CuerpoCuestionario a, CuerpoCuestionario b){
        return a.getCuerpoCuestionarioPK().getSintomatologia().getNombre().compareTo(b.getCuerpoCuestionarioPK().getSintomatologia().getNombre());
    }

}
