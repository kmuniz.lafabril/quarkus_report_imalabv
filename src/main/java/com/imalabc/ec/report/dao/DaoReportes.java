package com.imalabc.ec.report.dao;

import com.imalabc.ec.report.object.CatalogoSexo;
import com.imalabc.ec.report.object.IteracionReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@ApplicationScoped
public class DaoReportes {

    @Inject
    EntityManager em;

    private Logger log = LoggerFactory.getLogger(DaoReportes.class);

    @Transactional
    public List<IteracionReport> getPrimerContagio(){
        List<IteracionReport> iteraciones = new ArrayList<>();
        try {
            //<editor-fold defaultstate="collapsed" desc="Query">
            Query query = em.createNativeQuery("select vl.id_valora, cu.id_cuesti, pl.nacimiento_paciente, pl.sexo_paciente, pc.incidencia, " +
                    "prl.name_parroq as parroquia, cl.name_canton as canton, pvl.name_provin as provincia " +
                    "from laboratory.primer_contagio pc " +
                    "inner join laboratory.valora_labora vl on pc.id_valora = vl.id_valora " +
                    "inner join laboratory.cuesti_labora cu on vl.id_cuestionario = cu.id_cuesti " +
                    "inner join laboratory.paciente_labora pl on cu.id_pacient_cuesti = pl.id_paciente " +
                    "inner join laboratory.parroq_labora prl on pl.id_parroq  = prl.id_parroq " +
                    "inner join laboratory.canton_labora cl on prl.id_canton = cl.id_canton " +
                    "inner join laboratory.provin_labora pvl on cl.id_provinc = pvl.id_provin " +
                    "order by DATE(pc.incidencia) desc;");
            //</editor-fold>
            List<Object[]> listFila = query.getResultList();
            listFila.forEach((columns) -> {
                iteraciones.add( new IteracionReport(Long.valueOf(columns[0].toString()), Long.valueOf(columns[1].toString()), LocalDate.parse(columns[2].toString()),
                        (Integer.valueOf(columns[3].toString()) == 0 ? CatalogoSexo.Hombre : CatalogoSexo.Mujer), LocalDate.parse(columns[4].toString()),
                        columns[5].toString(), columns[6].toString(), columns[7].toString()));
            });
        }catch (Exception e){
            log.error(e.toString());
        }
        return iteraciones;
    }

    @Transactional
    public List<IteracionReport> getPrimerAnticuerpo(){
        List<IteracionReport> iteraciones = new ArrayList<>();
        try {
            //<editor-fold defaultstate="collapsed" desc="Query">
            Query query = em.createNativeQuery("select vl.id_valora, cu.id_cuesti, pl.nacimiento_paciente, pl.sexo_paciente, pc.incidencia, " +
                    "prl.name_parroq as parroquia, cl.name_canton as canton, pvl.name_provin as provincia " +
                    "from laboratory.primer_anticuerpo pc " +
                    "inner join laboratory.valora_labora vl on pc.id_valora = vl.id_valora " +
                    "inner join laboratory.cuesti_labora cu on vl.id_cuestionario = cu.id_cuesti " +
                    "inner join laboratory.paciente_labora pl on cu.id_pacient_cuesti = pl.id_paciente " +
                    "inner join laboratory.parroq_labora prl on pl.id_parroq  = prl.id_parroq " +
                    "inner join laboratory.canton_labora cl on prl.id_canton = cl.id_canton " +
                    "inner join laboratory.provin_labora pvl on cl.id_provinc = pvl.id_provin " +
                    "order by DATE(pc.incidencia) desc;");
            //</editor-fold>
            List<Object[]> listFila = query.getResultList();
            listFila.forEach((columns) -> {
                iteraciones.add( new IteracionReport(Long.valueOf(columns[0].toString()), Long.valueOf(columns[1].toString()), LocalDate.parse(columns[2].toString()),
                        (Integer.valueOf(columns[3].toString()) == 0 ? CatalogoSexo.Hombre : CatalogoSexo.Mujer), LocalDate.parse(columns[4].toString()),
                        columns[5].toString(), columns[6].toString(), columns[7].toString()));
            });
        }catch (Exception e){
            log.error(e.toString());
        }
        return iteraciones;
    }

    @Transactional
    public List<IteracionReport> getSegundoContagio(){
        List<IteracionReport> iteraciones = new ArrayList<>();
        try {
            //<editor-fold defaultstate="collapsed" desc="Query">
            Query query = em.createNativeQuery("select vl.id_valora, cu.id_cuesti, pl.nacimiento_paciente, pl.sexo_paciente, sc.incidencia, " +
                    "prl.name_parroq as parroquia, cl.name_canton as canton, pvl.name_provin as provincia " +
                    "from laboratory.segundo_contagio sc " +
                    "inner join laboratory.valora_labora vl on sc.id_valora = vl.id_valora " +
                    "inner join laboratory.cuesti_labora cu on vl.id_cuestionario = cu.id_cuesti " +
                    "inner join laboratory.paciente_labora pl on cu.id_pacient_cuesti = pl.id_paciente " +
                    "inner join laboratory.parroq_labora prl on pl.id_parroq  = prl.id_parroq " +
                    "inner join laboratory.canton_labora cl on prl.id_canton = cl.id_canton " +
                    "inner join laboratory.provin_labora pvl on cl.id_provinc = pvl.id_provin " +
                    "order by DATE(sc.incidencia) desc;");
            //</editor-fold>
            List<Object[]> listFila = query.getResultList();
            listFila.forEach((columns) -> {
                iteraciones.add( new IteracionReport(Long.valueOf(columns[0].toString()), Long.valueOf(columns[1].toString()), LocalDate.parse(columns[2].toString()),
                        (Integer.valueOf(columns[3].toString()) == 0 ? CatalogoSexo.Hombre : CatalogoSexo.Mujer), LocalDate.parse(columns[4].toString()),
                        columns[5].toString(), columns[6].toString(), columns[7].toString()));
            });
        }catch (Exception e){
            log.error(e.toString());
        }
        return iteraciones;
    }

    @Transactional
    public List<IteracionReport> getSegundoAnticuerpo(){
        List<IteracionReport> iteraciones = new ArrayList<>();
        try {
            //<editor-fold defaultstate="collapsed" desc="Query">
            Query query = em.createNativeQuery("select vl.id_valora, cu.id_cuesti, pl.nacimiento_paciente, pl.sexo_paciente, sc.incidencia, " +
                    "prl.name_parroq as parroquia, cl.name_canton as canton, pvl.name_provin as provincia " +
                    "from laboratory.segundo_anticuerpo sc " +
                    "inner join laboratory.valora_labora vl on sc.id_valora = vl.id_valora  " +
                    "inner join laboratory.cuesti_labora cu on vl.id_cuestionario = cu.id_cuesti " +
                    "inner join laboratory.paciente_labora pl on cu.id_pacient_cuesti = pl.id_paciente " +
                    "inner join laboratory.parroq_labora prl on pl.id_parroq  = prl.id_parroq " +
                    "inner join laboratory.canton_labora cl on prl.id_canton = cl.id_canton " +
                    "inner join laboratory.provin_labora pvl on cl.id_provinc = pvl.id_provin " +
                    "order by DATE(sc.incidencia) desc;");
            //</editor-fold>
            List<Object[]> listFila = query.getResultList();
            listFila.forEach((columns) -> {
                iteraciones.add( new IteracionReport(Long.valueOf(columns[0].toString()), Long.valueOf(columns[1].toString()), LocalDate.parse(columns[2].toString()),
                        (Integer.valueOf(columns[3].toString()) == 0 ? CatalogoSexo.Hombre : CatalogoSexo.Mujer), LocalDate.parse(columns[4].toString()),
                        columns[5].toString(), columns[6].toString(), columns[7].toString()));
            });
        }catch (Exception e){
            log.error(e.toString());
        }
        return iteraciones;
    }

    @Transactional
    public List<IteracionReport> getContagioActual(){
        List<IteracionReport> iteraciones = new ArrayList<>();
        try {
            //<editor-fold defaultstate="collapsed" desc="Query">
            Query query = em.createNativeQuery("select vl.id_valora, cu.id_cuesti, pl.nacimiento_paciente, pl.sexo_paciente, ac.incidencia, " +
                    "prl.name_parroq as parroquia, cl.name_canton as canton, pvl.name_provin as provincia " +
                    "from laboratory.actual_contagio ac " +
                    "inner join laboratory.valora_labora vl on ac.id_valora = vl.id_valora " +
                    "inner join laboratory.cuesti_labora cu on vl.id_cuestionario = cu.id_cuesti " +
                    "inner join laboratory.paciente_labora pl on cu.id_pacient_cuesti = pl.id_paciente " +
                    "inner join laboratory.parroq_labora prl on pl.id_parroq  = prl.id_parroq " +
                    "inner join laboratory.canton_labora cl on prl.id_canton = cl.id_canton " +
                    "inner join laboratory.provin_labora pvl on cl.id_provinc = pvl.id_provin " +
                    "order by DATE(ac.incidencia) desc;");
            //</editor-fold>
            List<Object[]> listFila = query.getResultList();
            listFila.forEach((columns) -> {
                iteraciones.add( new IteracionReport(Long.valueOf(columns[0].toString()), Long.valueOf(columns[1].toString()), LocalDate.parse(columns[2].toString()),
                        (Integer.valueOf(columns[3].toString()) == 0 ? CatalogoSexo.Hombre : CatalogoSexo.Mujer), LocalDate.parse(columns[4].toString()),
                        columns[5].toString(), columns[6].toString(), columns[7].toString()));
            });
        }catch (Exception e){
            log.error(e.toString());
        }
        return iteraciones;
    }

}
