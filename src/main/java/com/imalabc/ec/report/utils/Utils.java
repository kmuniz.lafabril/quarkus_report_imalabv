package com.imalabc.ec.report.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@ApplicationScoped
public class Utils {

    public Utils() {
    }

    public static void logCreate(String type, Object error) {
        System.out.println(type.toUpperCase().concat(": ").concat(error.toString()));
    }

    public static String toMaxLongitud(String data, int longitud) {
        return data.length() > longitud ? data.substring(0, longitud - 1) : data;
    }

    public static boolean doubleInRange(Double in) {
        String[] descPrice = in.toString().replace(".", "ERROR").split("ERROR", 3);
        return descPrice[0].length() > 8 || descPrice[1].length() > 2;
    }

    public static boolean isNumeric(String strNum) {
        boolean esNumeroPositivo = false;

        try {
            Long d = Long.parseLong(strNum);
            if (d > 0L) {
                esNumeroPositivo = true;
            }
        } catch (NullPointerException | NumberFormatException var3) {
            esNumeroPositivo = false;
        }

        return esNumeroPositivo;
    }

    public static boolean isLongitudMinimo(String tag, int longMin) {
        return tag.length() < longMin;
    }

    public static boolean isLongitudMaximo(String tag, int longMax) {
        return tag.length() > longMax;
    }

    public static Response validarObjeto(Object in) {
        return in == null ? ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST, MenssageResponse.entidadUnkload(in.getClass().getName())) : validarAtributosObjeto(in);
    }

    public static Response validarAtributosObjeto(Object object) {
        Method[] metodos = object.getClass().getMethods();
        Method[] var2 = metodos;
        int var3 = metodos.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Method metodo = var2[var4];
            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
                try {
                    if (metodo.getReturnType().equals(Byte.class) && (Byte)metodo.invoke(object) == null || metodo.getReturnType().equals(BigDecimal.class) && (BigDecimal)metodo.invoke(object) == null || metodo.getReturnType().equals(Double.class) && (Double)metodo.invoke(object) == null || metodo.getReturnType().equals(String.class) && ((String)metodo.invoke(object) == null || "".equals((String)metodo.invoke(object))) || metodo.getReturnType().equals(Date.class) && (Date)metodo.invoke(object) == null || metodo.getReturnType().equals(LocalDate.class) && (LocalDate)metodo.invoke(object) == null || metodo.getReturnType().equals(LocalTime.class) && (LocalTime)metodo.invoke(object) == null || metodo.getReturnType().equals(LocalDateTime.class) && (LocalDateTime)metodo.invoke(object) == null || metodo.getReturnType().equals(Enum.class) && (Enum)metodo.invoke(object) == null) {
                        return ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST, MenssageResponse.campoVacio(getNamePropertyObjetc(metodo.getName())));
                    }
                } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException var7) {
                    logCreate("ERROR", var7.getMessage() != null ? var7.getMessage() : var7);
                    return ResponseStandarBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, MenssageResponse.errorInterno(), var7);
                }
            }
        }

        return null;
    }

    public static String getNamePropertyObjetc(String getProperty) {
        return "".equals(getProperty) ? "DESCONOCIDO" : (getProperty.contains("get") ? getProperty.replace("get", "").toUpperCase() : getProperty.replace("is", "").toUpperCase());
    }

    public static Object validateMicroServ(Response respMicSer, Class<?> classOut) {
        ServerResponse serverResponse = null;
        Object result = new Object();

        try {
            if (respMicSer.getStatus() == 200) {
                serverResponse = (ServerResponse)respMicSer.readEntity(ServerResponse.class);
                ObjectMapper mapper = new ObjectMapper();
                result = mapper.convertValue(serverResponse.getRespuesta(), classOut);
            }
        } catch (IllegalArgumentException var5) {
            System.out.println(var5);
        }

        return result;
    }

    public static String capitaliceCadena(String message) {
        char[] caracteres = message.toCharArray();
        caracteres[0] = Character.toUpperCase(caracteres[0]);

        for(int i = 0; i < message.length() - 2; ++i) {
            if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ',') {
                caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
            }
        }

        return new String(caracteres);
    }

}
