package com.imalabc.ec.report.utils;

import javax.enterprise.context.ApplicationScoped;
import java.util.Iterator;
import java.util.List;

@ApplicationScoped
public class MenssageResponse {

    public MenssageResponse() {
    }

    public static String sinResgistro(String entidad) {
        return "No se encontraron registros.".concat(" En la entidad ").concat(entidad.toUpperCase());
    }

    public static String getExitoso(String entidad) {
        return "Se realizó la consulta exitosamente.".concat(" En la entidad ").concat(entidad.toUpperCase());
    }

    public static String errorInterno() {
        return "Algo salió mal, por favor comunícate con el Administrador del sistema";
    }

    public static String errorApiExterno(String entidad, String messageError) {
        return "No se logro la cominicación con la API: ".concat(entidad.toUpperCase()).concat(". Causa").concat(messageError);
    }

    public static String errorConversionEntity(String messageError) {
        return "No se logró convertir a la entidad. ".concat(". Causa").concat(messageError);
    }

    public static String createEntity(String entidad) {
        return "Se registró exitosamente la nueva entidad. ".concat(" ").concat(entidad.toUpperCase());
    }

    public static String updateEntity(String entidad) {
        return "Se actualizo exitosamente la entidad. ".concat(" ").concat(entidad.toUpperCase());
    }

    public static String deleteEntity(String entidad) {
        return "Se eliminó exitosamente la entidad. ".concat(" ").concat(entidad.toUpperCase());
    }

    public static String repeatEntity(String entidad) {
        return "El registro ya existe en la entidad. ".concat(" ").concat(entidad.toUpperCase());
    }

    public static String campoVacio(String nameTag) {
        return "El campo no puede estar vacío:".concat(" ").concat(nameTag.toUpperCase());
    }

    public static String campoIncorrecto(String nameTag, String tipo) {
        return "El campo debe ser Tipo:".concat(" ".concat(tipo.toUpperCase())).concat(". ").concat(nameTag.toUpperCase());
    }

    public static String camposVacios(List<String> listNameTag) {
        String out = "Los campos no pueden estar vacio:".concat(" ");
        int contandor = 0;

        for(Iterator var3 = listNameTag.iterator(); var3.hasNext(); ++contandor) {
            String item = (String)var3.next();
            out = out + (contandor == 0 ? item : " ó ".concat(item));
        }

        return out;
    }

    public static String entidadUnkload(String entidad) {
        return "No se encuentra la entidad:".concat(" ").concat(entidad.toUpperCase());
    }

    public static String longitudMinimo(String tag, int longMin) {
        return "Longitud mínima en el campo: ".concat(tag.toUpperCase()).concat(". Lóngitud Minima: ".concat(longMin + ""));
    }

    public static String longitudExcedida(String tag, int longitud) {
        return "No puede exceder la longitud de ".concat(longitud + "").concat(" en el campo").concat(tag.toUpperCase());
    }

    public static String longitudExcedidaDouble(String tag) {
        return "No puede exceder la longitud de ".concat(" ENTERO: 8 DECIMAL: 2").concat(" en el campo ").concat(tag.toUpperCase());
    }

    public static String inconsistenciaActives(String entidad) {
        return "Inconsistencia de elementos activos.".concat(" ENTIDAD: ").concat(entidad.toUpperCase());
    }

    public static String loginUserExits() {
        return "La cedula del usuario ya se encuentra registrada";
    }

    public static String invalidPassword() {
        return "La contraseña, no cumple con los requisitos básicos";
    }

    public static String emailUserExits() {
        return "El correo electrónico del usuario ya se encuentra registrado";
    }

    public static String createUserAndVerify(String entidad) {
        return "Se registró exitosamente la nueva entidad. ".concat(" ").concat(entidad.toUpperCase()).concat("Se envió su clave de activación al correo electrónico registrado");
    }

}
