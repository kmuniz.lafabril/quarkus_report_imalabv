package com.imalabc.ec.report.utils;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class ResponseStandarBuilder {

    public ResponseStandarBuilder() {
    }

    public static Response responseSingle(Response.Status status, String message) {
        return Response.status(status).entity(new ServerResponse(status.getStatusCode(), status.name(), message)).build();
    }

    public static Response responseObject(Response.Status status, String message, Object entity) {
        return Response.status(status).entity(new ServerResponse(status.getStatusCode(), status.name(), message, entity)).build();
    }

    public static Response responseNullSingle(Object entity) {
        return (Response) Optional.ofNullable(entity).map((result) -> {
            return responseSingle(Response.Status.OK, MenssageResponse.getExitoso(entity.getClass().getName()));
        }).orElse(responseSingle(Response.Status.BAD_REQUEST, MenssageResponse.sinResgistro(entity.getClass().getName())));
    }

    public static Response responseNullObject(Object entity, String nameEntity) {
        List<Object> list = (List<Object>) entity;
        return (Response)Optional.ofNullable(list).map((result) -> {
            return responseObject(Response.Status.OK, MenssageResponse.getExitoso(nameEntity), list);
        }).orElse(responseObject(Response.Status.BAD_REQUEST, MenssageResponse.sinResgistro(nameEntity), list));
    }

}
