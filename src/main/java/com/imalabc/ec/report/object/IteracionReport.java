package com.imalabc.ec.report.object;

import com.imalabc.ec.report.domain.CuerpoCuestionario;

import java.time.LocalDate;
import java.util.List;

public class IteracionReport {

    private Long idValoracion;
    private Long idCuestionario;
    private LocalDate fechaNacmiento;
    private CatalogoSexo sexoPaciente;
    private LocalDate incidencia;
    private String parroquia;
    private String canton;
    private String provincia;

    private List<CuerpoCuestionario> cuerpoCuestioario;

    public IteracionReport() {
    }

    public IteracionReport(Long idValoracion, Long idCuestionario, LocalDate fechaNacmiento, CatalogoSexo sexoPaciente, LocalDate incidencia, String parroquia, String canton, String provincia) {
        this.idValoracion = idValoracion;
        this.idCuestionario = idCuestionario;
        this.fechaNacmiento = fechaNacmiento;
        this.sexoPaciente = sexoPaciente;
        this.incidencia = incidencia;
        this.parroquia = parroquia;
        this.canton = canton;
        this.provincia = provincia;
    }

    public Long getIdValoracion() {
        return idValoracion;
    }

    public void setIdValoracion(Long idValoracion) {
        this.idValoracion = idValoracion;
    }

    public Long getIdCuestionario() {
        return idCuestionario;
    }

    public void setIdCuestionario(Long idCuestionario) {
        this.idCuestionario = idCuestionario;
    }

    public LocalDate getFechaNacmiento() {
        return fechaNacmiento;
    }

    public void setFechaNacmiento(LocalDate fechaNacmiento) {
        this.fechaNacmiento = fechaNacmiento;
    }

    public CatalogoSexo getSexoPaciente() {
        return sexoPaciente;
    }

    public void setSexoPaciente(CatalogoSexo sexoPaciente) {
        this.sexoPaciente = sexoPaciente;
    }

    public LocalDate getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(LocalDate incidencia) {
        this.incidencia = incidencia;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public List<CuerpoCuestionario> getCuerpoCuestioario() {
        return cuerpoCuestioario;
    }

    public void setCuerpoCuestioario(List<CuerpoCuestionario> cuerpoCuestioario) {
        this.cuerpoCuestioario = cuerpoCuestioario;
    }

}
