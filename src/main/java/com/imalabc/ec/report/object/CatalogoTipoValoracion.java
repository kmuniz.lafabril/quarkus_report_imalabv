package com.imalabc.ec.report.object;

public enum CatalogoTipoValoracion {

    Igg_Igm(0), PCR(1);

    private final int value;

    private CatalogoTipoValoracion(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}
