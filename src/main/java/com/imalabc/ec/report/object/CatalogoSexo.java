package com.imalabc.ec.report.object;

public enum CatalogoSexo {

    Hombre(0), Mujer(1);

    private final int value;

    private CatalogoSexo(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}
