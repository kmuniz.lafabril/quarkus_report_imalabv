package com.imalabc.ec.report.object;

public enum CatalogoTipoSintoma {

    Sintoma(0), Enfermedad(1);

    private final int value;

    private CatalogoTipoSintoma(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}
