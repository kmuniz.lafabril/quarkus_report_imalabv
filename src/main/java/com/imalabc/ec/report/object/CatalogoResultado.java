package com.imalabc.ec.report.object;

public enum CatalogoResultado {

    NEGATIVO(0), POSITIVO(1);

    private final int value;

    private CatalogoResultado(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}