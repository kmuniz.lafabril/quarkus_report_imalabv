package com.imalabc.ec.report.management;

import com.imalabc.ec.report.dao.DaoReportes;
import com.imalabc.ec.report.object.IteracionReport;
import com.imalabc.ec.report.repository.CuerpoCuestionarioRepository;
import com.imalabc.ec.report.utils.MenssageResponse;
import com.imalabc.ec.report.utils.ResponseStandarBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static com.imalabc.ec.report.utils.ResponseStandarBuilder.responseObject;

@ApplicationScoped
public class ReporteManagement {

    @Inject
    DaoReportes daoReportes;

    @Inject
    CuerpoCuestionarioRepository cuestionarioRepository;

    private final Logger log = LoggerFactory.getLogger(ReporteManagement.class);


    public Response getPrimerContagio(){
        try{
            List<IteracionReport> iteraciones = new ArrayList<>();
            iteraciones = this.daoReportes.getPrimerContagio();
            iteraciones.forEach(p->p.setCuerpoCuestioario(this.cuestionarioRepository.findBodyByIdHead(p.getIdCuestionario())));
            return (iteraciones.isEmpty()) ?
                ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST,"Primer Contagio")
                : ResponseStandarBuilder.responseObject(Response.Status.OK,"Primer Contagio", iteraciones);
        } catch (Exception e){
            log.error(e.getMessage() != null ? e.getMessage() : e.toString());
            return responseObject(Response.Status.INTERNAL_SERVER_ERROR,
                    MenssageResponse.errorInterno(), e);
        }
    }

    public Response getPrimerAnticuerpo() {
        try{
            List<IteracionReport> iteraciones = new ArrayList<>();
            iteraciones = this.daoReportes.getPrimerAnticuerpo();
            iteraciones.forEach(p->p.setCuerpoCuestioario(this.cuestionarioRepository.findBodyByIdHead(p.getIdCuestionario())));
            return (iteraciones.isEmpty()) ?
                ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST,"Primer Anticuerpo")
                : ResponseStandarBuilder.responseObject(Response.Status.OK,"Primer Anticuerpo", iteraciones);
        } catch (Exception e){
            log.error(e.getMessage() != null ? e.getMessage() : e.toString());
            return responseObject(Response.Status.INTERNAL_SERVER_ERROR,
                    MenssageResponse.errorInterno(), e);
        }
    }

    public Response getSegundoContagio(){
        try{
            List<IteracionReport> iteraciones = new ArrayList<>();
            iteraciones = this.daoReportes.getSegundoContagio();
            iteraciones.forEach(p->p.setCuerpoCuestioario(this.cuestionarioRepository.findBodyByIdHead(p.getIdCuestionario())));
            return (iteraciones.isEmpty()) ?
                ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST,"Segundo Contagio")
                : ResponseStandarBuilder.responseObject(Response.Status.OK,"Segundo Contagio", iteraciones);
        } catch (Exception e){
            log.error(e.getMessage() != null ? e.getMessage() : e.toString());
            return responseObject(Response.Status.INTERNAL_SERVER_ERROR,
                    MenssageResponse.errorInterno(), e);
        }
    }

    public Response getSegundoAnticuerpo(){
        try{
            List<IteracionReport> iteraciones = new ArrayList<>();
            iteraciones = this.daoReportes.getSegundoAnticuerpo();
            iteraciones.forEach(p->p.setCuerpoCuestioario(this.cuestionarioRepository.findBodyByIdHead(p.getIdCuestionario())));
            return (iteraciones.isEmpty()) ?
                ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST,"Segundo Anticuerpo")
                : ResponseStandarBuilder.responseObject(Response.Status.OK,"Segundo Anticuerpo", iteraciones);
        } catch (Exception e){
            log.error(e.getMessage() != null ? e.getMessage() : e.toString());
            return responseObject(Response.Status.INTERNAL_SERVER_ERROR,
                    MenssageResponse.errorInterno(), e);
        }
    }

    public Response getContagioActual(){
        try{
            List<IteracionReport> iteraciones = new ArrayList<>();
            iteraciones = this.daoReportes.getContagioActual();
            iteraciones.forEach(p->p.setCuerpoCuestioario(this.cuestionarioRepository.findBodyByIdHead(p.getIdCuestionario())));
            return (iteraciones.isEmpty()) ?
                ResponseStandarBuilder.responseSingle(Response.Status.BAD_REQUEST,"Contagio Actaul")
                :   ResponseStandarBuilder.responseObject(Response.Status.OK,"Contagio Actual", iteraciones);
        } catch (Exception e){
            log.error(e.getMessage() != null ? e.getMessage() : e.toString());
            return responseObject(Response.Status.INTERNAL_SERVER_ERROR,
                    MenssageResponse.errorInterno(), e);
        }
    }

}
