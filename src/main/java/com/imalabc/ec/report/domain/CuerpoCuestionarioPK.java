package com.imalabc.ec.report.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CuerpoCuestionarioPK implements Serializable {

    @Column(name = "ID_CUESTI_HEAD")
    private Long cuestionario;

    @OneToOne
    @JoinColumn(name = "ID_SINTOMA_CUESTI", referencedColumnName = "ID_SINTOM")
    private Sintomatologia sintomatologia;

    public CuerpoCuestionarioPK() {
    }

    public Long getCuestionario() {
        return cuestionario;
    }

    public void setCuestionario(Long cuestionario) {
        this.cuestionario = cuestionario;
    }

    public Sintomatologia getSintomatologia() {
        return sintomatologia;
    }

    public void setSintomatologia(Sintomatologia sintomatologia) {
        this.sintomatologia = sintomatologia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CuerpoCuestionarioPK)) return false;
        CuerpoCuestionarioPK that = (CuerpoCuestionarioPK) o;
        return getCuestionario().equals(that.getCuestionario()) && getSintomatologia().equals(that.getSintomatologia());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCuestionario(), getSintomatologia());
    }

    @Override
    public String toString() {
        return "CuerpoCuestionarioPK{" +
                "cuestionario=" + cuestionario +
                ", sintomatologia=" + sintomatologia +
                '}';
    }

}
