package com.imalabc.ec.report.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "CUERP_CUESTI_LABORA", schema = "LABORATORY")
@Cacheable(false)
public class CuerpoCuestionario implements Serializable {

    @EmbeddedId
    private CuerpoCuestionarioPK cuerpoCuestionarioPK = new CuerpoCuestionarioPK();

    @Basic
    @Column(name = "MARCA_CUESTI")
    private boolean marca;

    @Basic
    @Column(name = "OBSERV_CUESTI", length = 300)
    private String observacion;

    @Basic
    @Column(name = "NUM_DIA")
    private int numeroDia;

    public CuerpoCuestionario() {
    }

    public CuerpoCuestionario(boolean marca, String observacion, int numeroDia, Long cuestionario, Sintomatologia sintomatologia) {
        this.marca = marca;
        this.observacion = observacion;
        this.numeroDia = numeroDia;
        this.cuerpoCuestionarioPK.setCuestionario(cuestionario);
        this.cuerpoCuestionarioPK.setSintomatologia(sintomatologia);
    }

    public CuerpoCuestionarioPK getCuerpoCuestionarioPK() {
        return cuerpoCuestionarioPK;
    }

    public void setCuerpoCuestionarioPK(CuerpoCuestionarioPK cuerpoCuestionarioPK) {
        this.cuerpoCuestionarioPK = cuerpoCuestionarioPK;
    }

    public boolean isMarca() {
        return marca;
    }

    public void setMarca(boolean marca) {
        this.marca = marca;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getNumeroDia() {
        return numeroDia;
    }

    public void setNumeroDia(int numeroDia) {
        this.numeroDia = numeroDia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CuerpoCuestionario)) return false;
        CuerpoCuestionario that = (CuerpoCuestionario) o;
        return Objects.equals(getCuerpoCuestionarioPK(), that.getCuerpoCuestionarioPK());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCuerpoCuestionarioPK());
    }

    @Override
    public String toString() {
        return "CuerpoCuestionario{" +
                "cuerpoCuestionarioPK=" + cuerpoCuestionarioPK +
                ", marca=" + marca +
                ", observacion='" + observacion + '\'' +
                ", numeroDia=" + numeroDia +
                '}';
    }

}