package com.imalabc.ec.report.domain;

import com.imalabc.ec.report.object.CatalogoTipoSintoma;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "SINTOM_LABORA", schema = "LABORATORY")
@Cacheable(false)
public class Sintomatologia implements Serializable {

    @Id
    @GeneratedValue(generator = "seq_simtomatologia", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "seq_simtomatologia", sequenceName = "seq_simtomatologia", schema = "laboratory")
    @Column(name = "ID_SINTOM")
    private Long id;

    @Basic
    @Column(name = "SINTOM_NAME", length = 200)
    @NotNull
    @Size(min = 3, max = 200)
    private String nombre;

    @Basic
    @Column(name = "DIA_REQUER")
    private boolean diaRequerido;

    @Basic
    @Column(name = "TIPO_SINTOM")
    @Enumerated
    private CatalogoTipoSintoma tipoSintoma;

    @Basic
    @Column(name = "STATUS")
    private boolean estado;

    public Sintomatologia() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isDiaRequerido() {
        return diaRequerido;
    }

    public void setDiaRequerido(boolean diaRequerido) {
        this.diaRequerido = diaRequerido;
    }

    public CatalogoTipoSintoma getTipoSintoma() {
        return tipoSintoma;
    }

    public void setTipoSintoma(CatalogoTipoSintoma tipoSintoma) {
        this.tipoSintoma = tipoSintoma;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Objects.equals(getClass(), obj.getClass())) {
            return false;
        }
        final Sintomatologia other = (Sintomatologia) obj;
        if (!java.util.Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        if (!java.util.Objects.equals(this.getNombre(), other.getNombre())) {
            return false;
        }
        if (this.isDiaRequerido() != other.isDiaRequerido()) {
            return false;
        }
        if (!java.util.Objects.equals(this.getTipoSintoma(), other.getTipoSintoma())) {
            return false;
        }
        return this.isEstado() == other.isEstado();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.getId());
        hash = 31 * hash + Objects.hashCode(this.getNombre());
        hash = 31 * hash + (this.isDiaRequerido() ? 1 : 0);
        hash = 31 * hash + Objects.hashCode(this.getTipoSintoma());
        hash = 31 * hash + (this.isEstado() ? 1 : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Simtomatologia{" + " id=" + id + ", nombre=" + nombre + ", diaRequerido=" + diaRequerido + ", tipoSintoma=" + tipoSintoma + ", estado=" + estado + '}';
    }

}