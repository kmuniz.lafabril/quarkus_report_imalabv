package com.imalabc.ec.report.domain;

import com.imalabc.ec.report.object.CatalogoResultado;
import com.imalabc.ec.report.object.CatalogoTipoValoracion;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
@Table(name = "VALORA_LABORA", schema = "LABORATORY")
@Cacheable(false)
public class Valoracion implements Serializable {

    @Id
    @Column(name = "ID_VALORA")
    private Long id;

    @Basic
    @Column(name = "idPaciente")
    private Long paciente;

    @Basic
    @Column(name = "idResponsable")
    private Long responsable;

    @Basic
    @Column(name = "TIPO_VALORA")
    @Enumerated
    private CatalogoTipoValoracion tipoValoracion;

    @Basic
    @Column(name = "IGG_RESP")
    @Enumerated
    private CatalogoResultado igg;

    @Basic
    @Column(name = "IGM_RESP")
    @Enumerated
    private CatalogoResultado igm;

    @Basic
    @Column(name = "PCR_RESP")
    @Enumerated
    private CatalogoResultado pcr;

    @Basic
    @Column(name = "FECHA_MUESTRA")
    private LocalDate fechaMuestra;

    @Basic
    @Column(name = "FECHA_HORA_REGIST")
    private Timestamp fechaHoraRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPaciente() {
        return paciente;
    }

    public void setPaciente(Long paciente) {
        this.paciente = paciente;
    }

    public Long getResponsable() {
        return responsable;
    }

    public void setResponsable(Long responsable) {
        this.responsable = responsable;
    }

    public CatalogoTipoValoracion getTipoValoracion() {
        return tipoValoracion;
    }

    public void setTipoValoracion(CatalogoTipoValoracion tipoValoracion) {
        this.tipoValoracion = tipoValoracion;
    }

    public CatalogoResultado getIgg() {
        return igg;
    }

    public void setIgg(CatalogoResultado igg) {
        this.igg = igg;
    }

    public CatalogoResultado getIgm() {
        return igm;
    }

    public void setIgm(CatalogoResultado igm) {
        this.igm = igm;
    }

    public CatalogoResultado getPcr() {
        return pcr;
    }

    public void setPcr(CatalogoResultado pcr) {
        this.pcr = pcr;
    }

    public LocalDate getFechaMuestra() {
        return fechaMuestra;
    }

    public void setFechaMuestra(LocalDate fechaMuestra) {
        this.fechaMuestra = fechaMuestra;
    }

    public Timestamp getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Timestamp fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    @Override
    public String toString() {
        return "Valoracion{" +
                "id=" + id +
                ", paciente=" + paciente +
                ", responsable=" + responsable +
                ", tipoValoracion=" + tipoValoracion +
                ", igg=" + igg +
                ", igm=" + igm +
                ", pcr=" + pcr +
                ", fechaMuestra=" + fechaMuestra +
                ", fechaHoraRegistro=" + fechaHoraRegistro +
                '}';
    }
}