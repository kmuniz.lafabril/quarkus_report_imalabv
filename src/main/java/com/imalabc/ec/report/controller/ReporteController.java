package com.imalabc.ec.report.controller;

import com.imalabc.ec.report.management.ReporteManagement;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/api/")
@Produces(MediaType.APPLICATION_JSON)
public class ReporteController {

    @Inject
    ReporteManagement management;

    @Timed
    @Operation(summary = "get all the encuesta")
    @APIResponse(responseCode = "200", description = "OK")
    @Path("primer_contagio")
    @GET
    public Response getPrimerContagio() {
        return this.management.getPrimerContagio();
    }

    @Timed
    @Operation(summary = "get all the encuesta")
    @APIResponse(responseCode = "200", description = "OK")
    @Path("primer_anticuerpo")
    @GET
    public Response getPrimerAnticuerpo() {
        return this.management.getPrimerAnticuerpo();
    }

    @Timed
    @Operation(summary = "get all the encuesta")
    @APIResponse(responseCode = "200", description = "OK")
    @Path("segundo_contagio")
    @GET
    public Response getSegundoContagio() {
        return this.management.getSegundoContagio();
    }

    @Timed
    @Operation(summary = "get all the encuesta")
    @APIResponse(responseCode = "200", description = "OK")
    @Path("segundo_anticuerpo")
    @GET
    public Response getSegundoAnticuerpo() {
        return this.management.getSegundoAnticuerpo();
    }

    @Timed
    @Operation(summary = "get all the encuesta")
    @APIResponse(responseCode = "200", description = "OK")
    @Path("contagio_actual")
    @GET
    public Response getContagioActual() {
        return this.management.getContagioActual();
    }

}
